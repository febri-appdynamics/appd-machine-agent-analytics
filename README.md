## Machine Agent

### Summary
Analytics agent will fail to connect if Controller & Event Service is using a self-signed certificate.
The workaround is, import the Controller & Event Service certificate into the Analytics agent's TrustStore using keytool command these can be found in the `updateAnalyticsAgent.sh` script.

### Official AppDynamics Machine Agent Analytics images
The official Machine Agent Analytics images are available from the Docker Hub.

`appdynamics/machine-agent-analytics:latest` or

`appdynamics/machine-agent-analytics:4.5.9` to get a specific version of the agent

### AppDynamics Official Docker Images Source Repository
https://github.com/Appdynamics/appdynamics-docker-images

### Build this image

* Download the latest agent bundle from the [AppDynamics Official Download Site](https://download.appdynamics.com/download/) and place in this directory
* Make a note of sha256 checksum value for the download
* Rename the file to MachineAgent-x.x.x, substituing `x` with the version number, e.g. `MachineAgent-4.5.10.zip`
* Place your self-signed certificate in this root directory with `controller-cert.pem` name for Controller and `es-cert.pem` for Event Service self-signed certificate.
* Run ./build.sh passing the version, as it appears in the name of the zip file, and the checksum:
`./build.sh 4.5.10 d472dfc75469c`
* Or if you want skip the checksum:
`./build.sh 4.5.10`
 
 The checksum can be left blank to avoid validation. 
It is a best practice to validate the package integrity.

By default the image is tagged `appdynamics/machine-agent-analytics`. To change the image tag, pass the new name as the 3rd parameter to the build.sh or configure directly in build.sh script:
`./build.sh 4.5.10 d472dfc75469c `

### Push image to Container Registry
To deploy this image you will need to push this image into yout Container Registry first either Docker Hub or Private Container Regitry.
Example:
 * docker login
 * docker push febriyanto/machine-agent-analytics-self-signed-ssl:version-tag
